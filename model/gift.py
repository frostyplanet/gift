#coding:utf-8

import _env

from model.base.gid import gid, CID
from model._db import Model , ModelMc, redis, redis_key
from time import time
from misc.lib.base.base36 import b2a, a2b 
from uuid import uuid4
from abc import ABCMeta

#REDIS_XXX = redis_key.xxx("%s")


# 记得登记表名字 
# vi _table.py

__metaclass__ = type

class GIFT_STATE:
    RM = 0
    EXPIRE = 10
    USED = 20
    VAILD = 30

#class Gift(Model):
#    pass
#   @property
#   def code(self):
#        return b2a(self.uuid)

#   def expire_auto(self):
#        expire = self.expire
#       if expire and self.state == GIFT_STATE.VAILD and expire < time():
#           self.state = EXPIRE 
#           self.save() 


class GiftBase:
    __metaclass__ = ABCMeta

    CID = None
    @classmethod
    def new(cls, user_id, kind, expire=0):
        id = gid(CID.GIFT, user_id)
        while True:
            uuid = uuid4()
            if not Gift.get(uuid=uuid):
                break
        now = int(time())
        if expire:
            expire = now+expire
    #    gift = Gift(id=id, cid=cls.cid, kind=kind, user_id=user_id, state=GIFT_STATE.VAILD, time=now, uuid=uuid.bytes, expire=expire)
    #    gift.save()
    #    return gift 

    @classmethod
    def by_code(cls, code):
        uuid = a2b(code.upper()) 
        gift = Gift.get(uuid=uuid)
        gift.expire_auto()
        return gift


if __name__ == "__main__":
    pass
    CID.GIFT

