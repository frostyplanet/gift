#!/usr/bin/env python
# coding:utf-8

import _env
from model.gift.gift import GiftBase
from model.base.gid import gid, CID


class GiftVps(GiftBase):
    CID = CID.VPS


class GIFT_VPS_KIND:
    NEW = 1


if __name__ == "__main__":
    user_id = 0
    GiftVps.new(user_id, GIFT_VPS_KIND.NEW)
    print
